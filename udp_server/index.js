var udp = require("dgram");
const fetch = require('node-fetch');
const INFLUX_URL =  "http://software-analytics-g2.inf.usi.ch:8086/write?db=ardufana&u=admin&p=pass&precision=ms"
// --------------------creating a udp server --------------------
// creating a udp server
var server = udp.createSocket("udp4");

// emits when any error occurs
server.on("error", function(error) {
  console.log("Error: " + error);
  server.close();
});

// emits on new datagram msg
server.on("message", function(msg, info) {
  sendDataToInflux(msg.toString());
});


//emits when socket is ready and listening for datagram msgs
server.on('listening',function(){
    var address = server.address();
    var port = address.port;
    var family = address.family;
    var ipaddr = address.address;
    console.log('Server is listening at port' + port);
    console.log('Server ip :' + ipaddr);
    console.log('Server is IP4/IP6 : ' + family);
});

//emits after the socket is closed using socket.close();
server.on('close',function(){
    console.log('Socket is closed !');
  });
server.bind(2222);

function sendDataToInflux(encodedData){
    const [personCount, co2, airquality] = encodedData.trim().split(",");
    const timestamp =  new Date().getTime();
    if( personCount === undefined || co2 === undefined || airquality == undefined){
        console.log("Some data is undefined")
    }
    
    sendMeasurementToInflux("person_count", personCount,  timestamp);
    sendMeasurementToInflux("co2", co2,  timestamp);
    sendMeasurementToInflux("airquality", airquality,  timestamp);
}

function sendMeasurementToInflux(measurement, value, timestamp){
    console.log("sending", measurement, value, timestamp)
    fetch(INFLUX_URL, { method: 'POST', body: encodeInflux(measurement, value, timestamp) })
    .then(res => console.log(res.status))
}
function encodeInflux(measurement, value, timestamp){
    return `${measurement},device=arduino value=${value} ${timestamp}`;
}
