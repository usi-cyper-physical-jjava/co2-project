\newpage

# Results {#results}

In this section, we list our results regarding the potential correlation of the number of people in a room and the \cotwo&nbsp;levels. We describe our setup for the experiment and report our findings. We start by showing our Grafana dashboard, then we explain how the performance of our two gas sensors compares and present our use case study in room SI-007 at USI.

## Grafana dashboard

\includeimageFS{images/grafanadashboard.png}{Our Grafana Dashboard}{fig:ourgrafana}{0.9}

In figure \figref{fig:ourgrafana} we show the dashboard we created to visualize the data we collected.

We use Grafana Dashboard to visualize our continuously updated data. We are able to easily see spikes in the data when blowing on the \cotwo&nbsp;sensors, when testing the complete solution. In the same way, the person counter in the visualization changes when the sensor detects a person.

Grafana doesn't only let us stream live data but also provides easy ways to make aggregations to provide useful insights on the collected data. We can get the average of all data points of a sensor, or a even sliding average, which was needed to visualze the measurements of our more unstable \cotwo&nbsp;sensor (shown as PM10 in the visualization).

Grafana already converts our timestamps to a human-readable format, which made us soon notice that our VM's clock was running about three minutes late. We fixed the clock and were able to confirm with Grafana that the timestamps saved for each data point were correct.

## MQ-135 vs MG-811

\includeimageFS{images/co2_and_pm10.png}{Measurements of our two $\text{CO}_{\text{2}}$ sensors}{fig:comparco}{0.9}

It became soon clear that the measurements coming from the MQ-135 \cotwo&nbsp;sensor had way more spikes in short intervals and were much more unstable than our MG-811 measurements: This is clearly visible in \figref{fig:comparco}.

Overall, this is not surprising, since the MG-811 costs about 50 times as much as the MQ-135. Unfortunately, the cheaper sensor would be only useful in our solution for experiments with extreme changes, such as from 0 to 100 people in a relatively small room.

\newpage

## Case study

\includeimageFS{images/co2overtime.png}{$\text{CO}_{\text{2}}$ with a marker when people entered}{fig:cotime}{0.9}
\includeimageFS{images/pm10measurements.png}{$\text{CO}_{\text{2}}$ over the same timespan as in the previous figure}{fig:pm10time}{0.9}

We conducted our case study in SI-007, a room that is between 20 and 30 square meters. We prepared the room by keeping the door open for some time and no people inside. Our solution was setup and running. After about 16 minutes, we let 5 people in the room.
Thanks to this preparation step, the air quality changed by a noticeable margin before the people entered. This was needed because we didn't have more people available for the experiment, and wouldn't have had a noticeable impact otherwise.

In \figref{fig:cotime}, the vertical marker shows the moment when people started entering the room. Before this, the \cotwo&nbsp;levels were decreasing, probably due to the door being open. Letting people in and closing the door didn't immediately increase the concentration of \cotwo, but made it increase over time.

In \figref{fig:pm10time}, instead, we don't see such a clear pattern. This is because this second graph represents the measurements of our second, less precise sensor. There is only a slight increasing trend, but the data pattern itself is not clear. This sensor is probably only useful for large spikes and not for such a small change in the number of people.
