#!/bin/bash

FILES=(
    0_title.md
    1_project_description.md
    2_system_design.md
    3_obtained_results.md
    4_challenges.md
)
OUTPUT_NAME=report.pdf

# Options:
# - generate table of contents
# - input format is md
# - add section numbers
# - handle h1 as chapters
# - use documentclass article
# - LaTeX engine
# - output

cat ${FILES[@]} | pandoc \
    --from markdown+smart+raw_tex+raw_attribute \
    --number-sections \
    --top-level-division=chapter \
    --pdf-engine=xelatex \
    -o $OUTPUT_NAME

exit 0
