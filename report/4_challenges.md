\newpage

# Challenges {#challenges}

## Server installation

Installing InfluxDB and Grafana was not a huge problem. However, for some reason, running InfluxDB as a background service on CentOS 7 did not seem to work. We solved this issue by running InfluxDB as a Docker container.

Another issue we encountered was that the ports on our VM were blocked by the Firewall. When we figured this out, opening them was easy. The InfluxDB Docker container managed to open the port by itself.

## WiFi module

In our initial solution, one of the two boards should have collected the data from the other board via BLE, and then sent all the collected measurements via WiFi to InfluxDB.

We tested the performance of the ESP-8266 WiFi module (ESP) and tried to program it on the Arduino Nano BLE 33 board. As a first step, we connected the Arduino's 3.3v output and GND (ground) to a breadboard as the power support. Then we connected the RXD pin of the Arduino to RX pin of the ESP, and also the TXD pin of the Arduino to the TX pin of the ESP, so that the ESP would be able to send data to our laptop. Finally, we connected the GND and the VCC pins of the ESP to the 3.3v output, and the CH_PD of the ESP to ground.

\figref{fig:espsetup} shows the setup of the connection between the Arduino board and the ESP.

\includeimageFS{images/esp8266_setup.jpeg}{ESP8266 setup}{fig:espsetup}{0.5}

To program the ESP module in order to make it work as a WiFi gateway in our solution, we tried to use the "AT" protocol. Here we show are the commands we tried to use to command the ESP:

```
AT+CWMODE=3 // ESP client and host dual mode
AT+CWJAP="SSID","password" // connect to WIFI
AT+CIFSR // check local IP address
AT+CIPSTART="TCP","IP address",port // establish TCP connection
AT+CIPMODE=1 // unvarnished transmission mode
AT+CIPSEND // start sending data
"data we want to send"
AT+CIPMODE=0 // normal mode
AT+CIPCLOSE // close TCP connection
```

This worked (\textit{i.e.,} we received the data on our running \texttt{netcat} server) only once. We couldn't make the communication between the ESP and the server work for the rest of our attempts, all with the same configuration and setup.

So we trid to use UDP instead of TCP by simply changing the "AT+CIPSTART" command. In that case, we could easily connect the ESP to the server, but due to the characteristics of the UDP protocol, the performance sending and receiving the data were very unstable. Most of the time, we couldn't receive any data, or we received only part of it.

For these reasons, we decided to drop the ESP module and replace it with an Android application as a gateway.

## Person counting

One of the main features of our project is implementing a solution to count the number of people in a room.

The first issue we encountered when getting the person counter to work was that we didn't think about the issue of the sensor outputting 5V signals, while the Arduino Nano 33 BLE expects 3.3 V inputs (this also applied to other sensors).\
We were able to solve this problem in the end by using a voltage divider.

Another issue was that we originally wanted to use the Adruino board responsible for the person counting as our main board, a client (BLE central), the one responsible for conncting to the other board, that acted as a server (BLE peripheral). In order to achieve this, we wanted to run two threads: one for the person counting and one for the BLE connection handling.

However, the issue was that the number of measurements we could perform using the sensors dropped significantly, enough to force us to change our approach. We ended up making the person counting Arduino board act as a simple BLE peripheral, and the other board (with the \cotwo&nbsp;sensors) as the BLE central, since it had less timing critical functionality.
