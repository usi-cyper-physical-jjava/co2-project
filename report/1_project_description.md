# Project description {#description}

This project wants to explore the correlation between the levels of concentration of \cotwo&nbsp;in a room and the number of people present. The main idea is to develop a solution that uses IoT devices to measure both things.

\includeimageFS{images/projectsketch.png}{Sketch of our project idea}{fig:sketch}{0.6}

The initial project requirements specified to use a sensor to detect people entering and leaving the room. This sensor should measure the number of people with a maximum error of 20%. The location of this sensor can vary, but a good candidate would be next to a door.

Another sensor should measure the concentration of \cotwo&nbsp;in parts per million, and should be located in a spot where the air quality can be measured without interferences from air streams near doors or windows, and is therefore representative of the air quality in the room.

Our initial solution is: we use two ultrasound sensors to track people entering or exiting the room, and two additional sensors for air quality. The two tasks should be performed by different devices, and the two devices must communicate to each other via Bluetooth Low Energy (BLE).

The repository of this project is available on GitLab.\seeurl{https://gitlab.com/usi-cyper-physical-jjava/co2-project}
