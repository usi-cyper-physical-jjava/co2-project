---
documentclass: report
geometry:
- margin=2cm
mainfont: Fira Sans Regular
header-includes: |
    \usepackage{graphicx}
    \usepackage{wrapfig}
    \usepackage{xspace}
papersize: a4
keywords:
- code
- style
- development
---

\newcommand{\includeimageF}[3]{%
    \begin{figure}[h]
        \centering
        \includegraphics[width=\textwidth]{#1}
        \caption{#2\label{#3}}
    \end{figure}
}

\newcommand{\includeimageFS}[4]{%
    \begin{figure}[h]
        \centering
        \includegraphics[width=#4\textwidth]{#1}
        \caption{#2\label{#3}}
    \end{figure}
}

\newcommand{\includeimageR}[3]{%
    \begin{wrapfigure}{r}{0.5\textwidth}
        \centering
        \includegraphics[width=0.5\textwidth]{#1}
        \caption{#2\label{#3}}
    \end{wrapfigure}
}

\newcommand{\includeimageL}[3]{%
    \begin{wrapfigure}{l}{0.5\textwidth}
        \centering
        \includegraphics[width=0.5\textwidth]{#1}
        \caption{#2\label{#3}}
    \end{wrapfigure}
}

\newcommand{\includeimage}[3]{%
    \includeimageR{#1}{#2}{#3}
}

\newcommand{\cotwo}{$\text{CO}_{\text{2}}$}

\newcommand{\ie}{\textit{i.e.,}\xspace}
\newcommand{\eg}{\textit{e.g.,}\xspace}
\newcommand{\etc}{\textit{etc.}\xspace}
\newcommand{\etal}{\textit{et al.}}
\newcommand{\secref}[1]{Section~\ref{#1}\xspace}
\newcommand{\shortecref}[1]{Sec.~\ref{#1}\xspace}
\newcommand{\chapref}[1]{Chapter~\ref{#1}\xspace}
\newcommand{\appref}[1]{Appendix~\ref{#1}\xspace}
\newcommand{\figref}[1]{Figure~\ref{#1}\xspace}
\newcommand{\listref}[1]{Listing~\ref{#1}\xspace}
\newcommand{\tabref}[1]{Table~\ref{#1}\xspace}
\newcommand{\seeurl}[1]{\footnote{See \url{#1}}}
\newcommand{\formula}[1]{\emph{#1}}

\begin{titlepage}
\begin{center}
    \vskip8cm
    {\large{Cyber-Physical Software Engineering}}
    \vskip1mm
    {\large{Semester project report}}
    \vskip9cm
    \hrule height 1pt
    \vskip5mm
    {\Huge{$\text{CO}_{\text{2}}$ indoor monitoring and correlation with number of present persons}}
    \vskip5mm
    \hrule height 1pt
    \vskip2cm
    {\Large{Alexander Camenzind, Valerie Burgener, Jiacheng Qi,}}
    \vskip1mm
    {\Large{Jacopo Fidacaro, Aron Fiechter}}
    \vfill
    {Last updated \today}
\end{center}
\end{titlepage}

\thispagestyle{empty}
\clearpage
\pagenumbering{roman}
\pagenumbering{arabic}
\setcounter{page}{1}
