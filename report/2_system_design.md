\newpage

# System design {#design}

This section describes our system design, focusing on the hardware and software used for the implementation.

## Architecture

In \figref{fig:architecture} we show the complete architecture of our implementation. The two Arduino Nano 33 BLE boards are responsible for counting the number of people (board on the left) and measuring the \cotwo&nbsp;concentrations (other board). We use an Android application to read the values emitted by the board on the right (which is also our main board) and send them to a database (InfluxDB) running on a remote server.

\includeimageFS{images/architecture.jpg}{Implementation architecture}{fig:architecture}{0.7}

## Hardware used

In this section, we describe the architecture of our implementation and continue with a list of all the hardware components we used. For each component, we provide a short description and an image.

The component called ESP 8266 WiFi module was not used in the final implementation due to issues that we describe in \secref{challenges}.

### Arduino Nano Ble

\includeimageF{images/nano33ble.jpg}{Arduino Nano BLE board}{fig:nanoble}

We use two **Arduino Nano 33 BLE** boards in our setup. We call them "PersonCounter1234" and "JJAVA-DEVICE-1234-5678".

The person counter board is a simple BLE peripheral (server) that advertises the current count of people in a room. It is attached to two ultrasonic sensors that are used for detecting the movement of people in two directions to adjust the people count.

The JJAVA board is the main board; it is both a BLE central (client) and a BLE peripheral. It acts as a central by connecting to the person counter board and periodically reading the person count. The board is attached to two sensors that measure CO2 and advertises the two CO2 readings and the stored person count, thus acting also as a BLE peripheral.

The board is depicted in \figref{fig:nanoble}.

### MQ-135 Gas sensor

The MQ-135 is a dedicated gas sensor that can measure a variety of gases in the air (e.g., $\text{NH}_{\text{3}}$, $\text{NO}_{\text{x}}$, alcohol, Benzene, smoke, \cotwo).

We use this sensor to measure the concentration of \cotwo&nbsp;in the air. To read this value, we use a small library that is available on GitHub, written by G. Krocker.\seeurl{https://github.com/GeorgK/MQ135}
The library does not provide ways of measuring the concentrations of the other gases. The value is reported in PPM, but is very variable and seems to be unstable.

We show a photo of the sensor in \figref{fig:mq135}.

\includeimageFS{images/mqgas.png}{MQ-135 Gas sensor}{fig:mq135}{0.7}

\includeimageR{images/MG811.png}{MG-811 CO2 sensor}{fig:mg811}

### MG-811 CO2 sensor

MG-811 is another sensor that measures the concentration of \cotwo, but more accurately than the MQ-135. Our original idea was to use the two sensors in different parts of the room, but in the end, we use the two sensors to double-check our values.

Of course, since this second sensor is much more precise and stable, we trust it more than the other one.

To read the concentration values from this sensor, we use the example code provided by the manufacturer. The code is also featured in a guide which explains better how it works.\seeurl{https://www.elecfreaks.com/learn-en/microbitOctopus/sensor/octopus_ef04100.html}

When first measuring the \cotwo concentration, we need to read the voltage output of the sensor and use it to define the "zero" value of the reading. This should be done in moderately clean air.

We show a photo of the sensor in \figref{fig:mg811}.

### JJAVA board with MQ-135 and MG-811 sensors

Both sensors needed to be powered by 5 Volts, so our implementation resulted in this setup. Unfortunately, the battery module we were using ended up not delivering enough energy to power both boards, so we always had run the sensor connected to a serial port of our computers.

In \figref{fig:jjavasetup} we show our hardware implementation.

\includeimageFS{images/jjava.jpg}{Setup of the JJAVA board}{fig:jjavasetup}{0.7}

### HC-SR04 ultrasonic sensor on board "PersonCounter1234"

\includeimageFS{images/person_counter.jpg}{Person counter board setup}{fig:personcounter}{0.7}

We use a pair of HC-SR04 ultrasonic sensors to detect if a person entered or left the room. The sensors are placed one next to the other, as depicted in \figref{fig:personcounter}, where we show the complete setup of the second board.

This sensor uses ultrasonic waves &mdash; like bats do &mdash; to measure distances to objects. We placed the board with both sensors close to a door and managed to detect people entering and leaving the room, depending on the order of activation of the sensors.

\newpage

### ESP 8266 WiFi module

We planned to use this module to connect to a Wi-Fi network and upload the collected data to a database.

However, complications in using the board drove us away from using it. The libraries we were trying to use often reported successful transmission when nothing was being received on the other end (\textit{e.g.,} a local \texttt{netcat} server).

Because of these problems, we instead opted for the development of an Android application that would read the BLE data advertised by the JJAVA board.

The sensor is depicted in \figref{fig:esp8266}.

\includeimageFS{images/ESP8266-01.jpg}{AESP 8266 WiFi module}{fig:esp8266}{0.6}

\newpage

## Software

In this section, we describe all of the used and developed software.

### Arduino IDE

We are using the standard Arduino IDE to program our Arduino boards. It is an open-source IDE that is used for writing and compiling the code for Arduino modules and sensors. The code written on the IDE platform will ultimately generate a Hex File, which is then transferred and uploaded to the controller on the target board.

In our case, we upload the code to Arduino Nano 33 BLE boards. In most cases, the code we use to interact with the sensors and modules was found in online documentation and examples.

\includeimage{images/Arduino_IDE.png}{Arduino IDE with the blink example}{fig:arduinoide}

Our solution uses two main files, one for each board. Each file manages setting up the BLE interface, then starts advertising (waiting for BLE centrals to connect) or starts scanning for peripherals. Only when the connection is active, the boards start collecting data and updating their advertised BLE characteristics.

### Android App

The JJAVA Arduino board (our main board) acts as both a BLE central and peripheral. It reads the person count from the other board (PersonCount1234).

The original idea was to use the ESP-8266 WiFi module to send the data from the JJAVA board to our InfluxDB instance, but this idea was later scrapped due to difficulties in using the ESP module.

Therefore, we developed an Android application that acts as a BLE central and connects to the JJAVA Adruino board. It reads the data advertised by the JJAVA board (person count, two \cotwo&nbsp;measurements), and every time the it reads data, it immediately forwards that data to our InfluxDB instance.


### InfluxDB

We use InfluxDB as our persistence layer. It is an open-source NoSQL time-series database, which is perfect for our solution.

InfluxDB stores each data point with a timestamp, an associated set of tags, and a set of fields

In our case, the field represents the actual measurement reading values, while the tag represents the metadata to describe the measurements. Since we are interested in visualizing the results of our measurements, as well as historic data collected and various statistics, we will use Grafana.

Our instance of InfluxDB is running at http://software-analytics-g2.inf.usi.ch:8086/ (accessing it via a browser will return a 404 page, interaction is to be done via the API).

\newpage

### Grafana

\includeimageFS{images/showcase_grafana.jpg}{An example Grafana interface}{fig:grafana}{0.8}

Grafana is a database analysis and monitoring tool. It is used to create dashboard visualizations of key metrics that can give essential insights on data. \figref{fig:grafana} shows an example of a Grafana interface.

In our case, we run Grafana as a process on a server and access the interface through the browser. We created a dashboard to display live \cotwo&nbsp;measurements, and person counts with different graphs. We also show statistics for each measurement and aggregated values.

Our Grafana instance is available at http://software-analytics-g1:8080/.
