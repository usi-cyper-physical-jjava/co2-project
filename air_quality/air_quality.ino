/****************************************************************************************/
/*************************************** INCLUDES ***************************************/
/****************************************************************************************/
#include <ArduinoBLE.h>
#include "MQ135.h" // locally stored



/****************************************************************************************/
/*************************************** DEFINES ****************************************/
/****************************************************************************************/

/*--------------------------------------- MQ-135 ---------------------------------------*/
#define ANALOGPIN             (A0)      //  Analog PIN on Arduino Board for MQ-135
#define RZERO                 (206.85)  //  RZERO Calibration Value

/*--------------------------------------- MG-811 ---------------------------------------*/
#define MG_PIN                (A1)      // Analog PIN on Arduino Board for MG-811
#define DC_GAIN               (8.5)     // DC gain of amplifier
#define READ_SAMPLE_INTERVAL  (50)      // Number of samples taken in normal operation
#define READ_SAMPLE_TIMES     (5)       // Sampling interval (ms) in normal operation
#define ZERO_POINT_VOLTAGE    (0.252)   // Sensor output (V) for 400PPM CO2
#define REACTION_VOLTAGE      (0.030)   // Voltage drop of sensor going into 1000PPM CO2



/****************************************************************************************/
/************************************** CONSTANTS ***************************************/
/****************************************************************************************/

/*--------------------------------------- SERIAL ---------------------------------------*/
const bool IS_SERIAL_ACTIVE = true;

/*---------------------------------------- TIME ----------------------------------------*/
// Interval until next measurement, in ms
const long MEASURE_MILLIS_INTERVAL = 1000;
// Interval between BLE peripheral scans
const int BLE_SCAN_MILLIS_INTERVAL = 0;

/*---------------------------------------- BLE -----------------------------------------*/
// Name of this device (central)
const char DEVICE_NAME[] = "JJAVA-DEVICE-1234-5678";
// UUID of BLE service advertised by this device
const char JJAVA_SERVICE_UUID[] = "250e";
// UUIDs of BLE characteristics offered by this service
const char PM10_CHARACTERISTIC_UUID[] = "ab98";
const char CO2_CHARACTERISTIC_UUID[] = "de65";

// Name of other device (peripheral)
String PERIPHERAL_NAME = "PersonCounter1234";
// UUID of BLE service advertised by peripheral
const char PERSON_COUNTER_SERVICE_UUID[] = "360f";
// UUIDs of BLE characteristic offered by the service of the peripheral
const char COUNT_CHARACTERISTIC_UUID[] = "cf42";

/*--------------------------------------- MQ-135 ---------------------------------------*/
const int MQ135_MULTIPLY_FACTOR = 100000;

/*--------------------------------------- MG-811 ---------------------------------------*/
/* Two points are taken from the curve. With these two points, a line is formed which is
   "approximately equivalent" to the original curve.
   data format: { x, y, slope };
   point1: (lg400, 0.324),
   point2: (lg4000, 0.280)
   slope = ( reaction voltage ) / (log400 –log1000)
*/
const float CO2Curve[3] = {
  2.602,
  ZERO_POINT_VOLTAGE,
  (REACTION_VOLTAGE / (2.602 - 3))
};
const bool LOG_CO2_VOLTS = true;



/****************************************************************************************/
/************************************** VARIABLES ***************************************/
/****************************************************************************************/

/*---------------------------------------- TIME ----------------------------------------*/
// Measurement interval handling
// last time the mesurements were performed, in ms
long previousMillis = 0;



/****************************************************************************************/
/************************************** INSTANCES ***************************************/
/****************************************************************************************/

/*--------------------------------------- MQ-135 ---------------------------------------*/
// PM10 sensor
MQ135 pm10Sensor = MQ135(ANALOGPIN);

/*---------------------------------------- BLE -----------------------------------------*/
// BLE characteristics for the service offered by this device
// remote clients will be able to get notifications if this characteristic changes
BLEIntCharacteristic personCountCharacteristic(
  COUNT_CHARACTERISTIC_UUID,
  BLERead | BLENotify
);
BLEIntCharacteristic pm10Characteristic(PM10_CHARACTERISTIC_UUID, BLERead | BLENotify);
BLEIntCharacteristic co2Characteristic(CO2_CHARACTERISTIC_UUID, BLERead | BLENotify);



/****************************************************************************************/
/************************************** UTIL CODE ***************************************/
/****************************************************************************************/

/*--------------------------------------- SERIAL ---------------------------------------*/
template<class T> void printSerial(T data) {
  if (IS_SERIAL_ACTIVE) {
    Serial.print(data);
  }
}

template<class T> void printlnSerial(T data) {
  if (IS_SERIAL_ACTIVE) {
    Serial.println(data);
  }
}



/****************************************************************************************/
/************************************** SETUP CODE **************************************/
/****************************************************************************************/

/*------------------------------------- MAIN SETUP -------------------------------------*/
void setup() {
  // Initialize serial if active
  if (IS_SERIAL_ACTIVE) {
    Serial.begin(9600);
    while (!Serial);
  }

  // Setup central connection led
  pinMode(LEDR, OUTPUT);
  digitalWrite(LEDR, HIGH);

  // Setup peripheral connection led
  pinMode(LEDB, OUTPUT);
  digitalWrite(LEDB, HIGH);

  // Setup data update led
  pinMode(LEDG, OUTPUT);
  digitalWrite(LEDG, HIGH);

  // Setup BLE comm with other board
  setup_ble();

  // Setup MQ-135 sensor
  setup_mq135();

  // Setup MG-811 sensor
  // nothing to do.
}

/*------------------------------------- BLE SETUP --------------------------------------*/
void setup_ble() {
  // Begin BLE initialization
  if (!BLE.begin()) {
    printlnSerial("starting BLE failed!");
    while (1);
  }

  // Set device and local name
  BLE.setLocalName(DEVICE_NAME);
  BLE.setDeviceName(DEVICE_NAME);

  // Print own BLE address
  printSerial("Local address is: ");
  printlnSerial(BLE.address());

  // Create and set JJAVA service to BLE device
  BLEService jjavaService(JJAVA_SERVICE_UUID);
  BLE.setAdvertisedService(jjavaService);

  // Add characteristics to service, then add service to device
  jjavaService.addCharacteristic(personCountCharacteristic);
  jjavaService.addCharacteristic(pm10Characteristic);
  jjavaService.addCharacteristic(co2Characteristic);
  BLE.addService(jjavaService);

  // Write initial values of the characteristics
  personCountCharacteristic.writeValue(0);
  pm10Characteristic.writeValue(0);
  co2Characteristic.writeValue(0);

  // Start advertising
  BLE.advertise();
  printlnSerial("JJAVA device active, waiting for connections...");
}

/*------------------------------------ MQ-135 SETUP ------------------------------------*/
void setup_mq135() {
  float rzero = pm10Sensor.getRZero();
  delay(3000);
  printSerial("MQ135 RZERO Calibration Value : ");
  printlnSerial(rzero);
}

/****************************************************************************************/
/************************************** LOOP CODE ***************************************/
/****************************************************************************************/

/*------------------------------------- MAIN LOOP --------------------------------------*/
void loop() {
  central_connection();
}

/*-------------------------------- CENTRAL CONNECTION ----------------------------------*/
void central_connection() {
  // Wait for a BLE central to connect
  BLEDevice central = BLE.central();
  printlnSerial("Scanning for clients...");

  // If no central connected to the peripheral
  if (!central) {
    // Restart advertising
    printlnSerial("No central found. Retrying in 5 seconds...");
    BLE.stopAdvertise();
    BLE.advertise();
    delay(5000);
    return;
  }

  // Otherwise a central is connected to the peripheral
  printSerial("Connected to central: ");
  // Print the central's BT address and turn on LED to indicate connection
  printlnSerial(central.address());
  digitalWrite(LEDR, LOW);

  // While the central is connected
  while (central.connected()) {
    peripheral_connection(central);
  }

  // When the central disconnects, turn off the LED:
  digitalWrite(LEDR, HIGH);
  printSerial("Disconnected from central: ");
  printlnSerial(central.address());

  // Restart advertising
  BLE.stopAdvertise();
  BLE.advertise();
}

/*------------------------------- PERIPHERAL CONNECTION --------------------------------*/
void peripheral_connection(BLEDevice& central) {
  // Scan for a peripheral
  printlnSerial("");
  printSerial("Scanning for peripherals... ");
  BLE.scan();
  BLEDevice peripheral = BLE.available();

  // If no central connected to the peripheral
  if (!peripheral) {
    // Stop scanning and return
    printlnSerial("none found. Retrying...");
    delay(BLE_SCAN_MILLIS_INTERVAL);
    return;
  }

  // Peripheral found, stop scanning
  BLE.stopScan();
  printSerial("found. Address: ");
  printlnSerial(peripheral.address());

  // If the peripheral has no name or a wrong name
  if (!peripheral.hasLocalName()) {
    printlnSerial("Peripheral has no name. Retrying...");
    delay(BLE_SCAN_MILLIS_INTERVAL);
    return;
  }

  // Otherwise print name
  String peripheral_name = peripheral.localName();
  printSerial("Peripheral name: ");
  printlnSerial(peripheral_name);

  if (peripheral_name != PERIPHERAL_NAME) {
    printlnSerial("Wrong peripheral name. Retrying...");
    delay(BLE_SCAN_MILLIS_INTERVAL);
    return;
  }

  // Othwerwise we discovered the correct peripheral
  printlnSerial("Correct peripheral found.");

  // If connection to peripheral fails
  if (!peripheral.connect()) {
    printlnSerial("Connection to peripheral failed. Retrying...");
    delay(BLE_SCAN_MILLIS_INTERVAL);
    return;
  }

  // Otherwise turn on connection led (LEDB)
  digitalWrite(LEDB, LOW);
  printSerial("Connected to peripheral: ");
  printlnSerial(peripheral.address());

  // Discover service and characteristic, then keep polling for data and update own
  // characteristics
  peripheral_loop(central, peripheral);

  // When the central disconnects, turn off the LED:
  digitalWrite(LEDB, HIGH);
  printSerial("Disconnected from peripheral: ");
  printlnSerial(peripheral.address());
}

/*---------------------------------- PERIPHERAL LOOP -----------------------------------*/
void peripheral_loop(BLEDevice& central, BLEDevice& peripheral) {
  // If service discovery fails
  if (!peripheral.discoverService(PERSON_COUNTER_SERVICE_UUID)) {
    Serial.println("Service discovery failed. Disconnecting and retrying...");
    peripheral.disconnect();
    delay(BLE_SCAN_MILLIS_INTERVAL);
    return;
  }

  // Otherwise create characteristic
  BLECharacteristic personCountInt = peripheral.characteristic(COUNT_CHARACTERISTIC_UUID);

  // If subscription to characteristic fails
  if (!personCountInt.subscribe()) {
    Serial.println("Subscription to characteristic failed. Disconnecting and retrying...");
    peripheral.disconnect();
    delay(BLE_SCAN_MILLIS_INTERVAL);
    return;
  }

  // Otherwise, while the peripheral is connected
  while (central.connected() && peripheral.connected()) {
    long currentMillis = millis();
    // If the interval has passed, update values:
    if (currentMillis - previousMillis >= MEASURE_MILLIS_INTERVAL) {
      // Read new values
      digitalWrite(LEDG, LOW);
      int personCount = readPersonCount(personCountInt);
      int pm10 = readPm10();
      int co2 = readCo2();
      delay(50);
      digitalWrite(LEDG, HIGH);

      // Update BLE service characteristics and log measurements
      updateCharacteristics(personCount, pm10, co2);
      logMeasurements(central.localName(), peripheral.localName(), personCount, pm10, co2);

      // Update previous with current time to keep interval going
      previousMillis = currentMillis;
    }
  }

  // If central disconnected, disconnect also peripheral
  if (!central.connected()) {
    peripheral.disconnect();
  }
}


/*-------------------------- CONVERSION OF BYTE ARRAY TO INT ---------------------------*/
int byteArrayToInt(const byte bytes[], int len) {
  int value;
  std::memcpy(&value, bytes, len);
  return value;
}

/*--------------------------------- READ MEASUREMENTS ----------------------------------*/
int readPersonCount(BLECharacteristic& personCountInt) {
  personCountInt.read();
  int personCount = byteArrayToInt(personCountInt.value(), personCountInt.valueLength());
  return personCount;
}

int readPm10() {
  int pm10 = (int) (pm10Sensor.getPPM() * MQ135_MULTIPLY_FACTOR);
  return pm10;
}

int readCo2() {
  float co2Volts = MGRead(MG_PIN);
  if (LOG_CO2_VOLTS) {
    printSerial("MG-811: ");
    printSerial(co2Volts);
    printlnSerial(" V");
  }
  int co2 = (int) MGGetPercentage(co2Volts, CO2Curve);
  return co2;
}

/*------------------------------- UPDATE CHARACTERISTICS -------------------------------*/
void updateCharacteristics(int personCount, int pm10, int co2) {
  personCountCharacteristic.writeValue(personCount);
  pm10Characteristic.writeValue(pm10);
  co2Characteristic.writeValue(co2);
}

/*---------------------------------- LOG MEASUREMENTS ----------------------------------*/
void logMeasurements(
  String centralName,
  String peripheralName,
  int personCount,
  int pm10,
  int co2
) {
  printSerial("[central: ");
  printSerial(centralName);
  printSerial("][peripheral: ");
  printSerial(peripheralName);
  printSerial("\t personCount = ");
  printSerial(personCount);
  printSerial(",\t pm10 = ");
  printSerial(pm10);
  printSerial(",\t co2 = ");
  printlnSerial(co2);
}

/*--------------------------------------- MG-811 ---------------------------------------*/

/*
 * MGRead
 * Input:   mg_pin - analog channel
 * Output:  output of SEN-000007
 * Remarks: This function reads the output of SEN-000007
 */
float MGRead(int mg_pin) {
  int i;
  float v = 0;

  for (i = 0; i < READ_SAMPLE_TIMES; i++) {
    v += analogRead(mg_pin);
    delay(READ_SAMPLE_INTERVAL);
  }
  v = (v / READ_SAMPLE_TIMES) * 5 / 1024 ;
  return v;
}

/*
 * MQGetPercentage
 * Input:   volts   - SEN-000007 output measured in volts        
 *          pcurve  - pointer to the curve of the target gas
 * Output:  ppm of the target gas
 * Remarks: By using the slope and a point of the line. The x(logarithmic value of ppm)        
 *          of the line could be derived if y(MG-811 output) is provided. As it is a
 *          logarithmic coordinate, power of 10 is used to convert the result to non-logarithmic
 *          value.
 */
int  MGGetPercentage(float volts, const float *pcurve) {
  return pow(10, ((volts / DC_GAIN) - pcurve[1]) / pcurve[2] + pcurve[0]);
}
