/****************************************************************************************/
/*************************************** INCLUDES ***************************************/
/****************************************************************************************/
#include <ArduinoBLE.h>
#include <NewPing.h>



/****************************************************************************************/
/*************************************** DEFINES ****************************************/
/****************************************************************************************/

#define TRIGGER_PIN_1  12
#define ECHO_PIN_1     11
#define TRIGGER_PIN_2  5
#define ECHO_PIN_2     6
#define MAX_DISTANCE 300 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.
const int THRESHOLD = 50; //< 50 cm close



/****************************************************************************************/
/************************************** CONSTANTS ***************************************/
/****************************************************************************************/

/*--------------------------------------- SERIAL ---------------------------------------*/
const bool IS_SERIAL_ACTIVE = true;

/*---------------------------------------- BLE -----------------------------------------*/
// Name of this device (peripheral)
const char DEVICE_NAME[] = "PersonCounter1234";
// UUID of BLE service advertised by this device
const char PERSON_COUNTER_SERVICE_UUID[] = "360f";
// UUIDs of BLE characteristic offered by the service
const char COUNT_CHARACTERISTIC_UUID[] = "cf42";



/****************************************************************************************/
/************************************** UTIL CODE ***************************************/
/****************************************************************************************/
template<class T> void printSerial(T data) {
  if (IS_SERIAL_ACTIVE) {
    Serial.print(data);
  }
}

template<class T> void printSerial(T data, int precision) {
  if (IS_SERIAL_ACTIVE) {
    Serial.print(data, precision);
  }
}

template<class T> void printlnSerial(T data) {
  if (IS_SERIAL_ACTIVE) {
    Serial.println(data);
  }
}



/****************************************************************************************/
/*************************************** CLASSES ****************************************/
/****************************************************************************************/

/*----------------------------------- PERSON COUNTER ------------------------------------*/
class SonicSensor {
  public:
    SonicSensor(uint8_t triggerPin, uint8_t echoPin) {
      sensor = new NewPing(triggerPin, echoPin, MAX_DISTANCE);
    }
    void measure() {
      int val = sensor->ping_cm();
      if (val != 0) {
        if (t_enter == -1 && last_dist > THRESHOLD && val < THRESHOLD ) {
          t_enter = millis();
          measurement_complete = false;
        } else if (t_enter != -1 && last_dist < THRESHOLD && val > THRESHOLD) {
          t_leave = millis();
          measurement_complete = true;
        }

        last_dist = val;
      }

    }
    boolean isMeasurmentComplete() {
      return measurement_complete;
    }
    void printStuff() {
      printSerial(" t_enter: ");
      printSerial(t_enter);
      printSerial(" t_leave: ");
      printSerial(t_leave);
      printSerial(" measurement_complete: ");
      printSerial(measurement_complete);
      printSerial(" last_dist: ");
      printlnSerial(last_dist);
    }
    //if times are older than 2 seconds we reset
    void checkTimeout() {
      if ( t_enter != -1 && millis() - t_enter > 2000) {
        reset();
      }
    }
    void reset() {
      t_enter = -1;
      t_leave = -1;
      measurement_complete = false;
    }
    int t_enter = -1;
    int t_leave = -1;
  private:
    NewPing* sensor;
    int last_dist = -1;
    boolean measurement_complete = false;

};

class PersonCounter {
  public:
    PersonCounter(BLEIntCharacteristic* counterCharacteristic) {
      s1 = new SonicSensor(TRIGGER_PIN_1, ECHO_PIN_1);
      s2 = new SonicSensor(TRIGGER_PIN_2, ECHO_PIN_2);
      _counterCharacteristic = counterCharacteristic;
    }

    void personCounterLoop() {
      if (!ready_to_measure && millis() - last_counted < 500) { //avoid legs and hands
        return;
      }
      ready_to_measure = true;
      delay(20);
      s1->measure();

      delay(20);
      s2->measure();

      checkCounter();
      s1->checkTimeout();
      s2->checkTimeout();
      printSerial("count: ");
      printSerial(counter);
      printlnSerial("");


    }

  private:
    void checkCounter() {
      if (s1->isMeasurmentComplete() && s2->isMeasurmentComplete()) {
        if (s1->t_enter < s2->t_enter ) {
          counter++;
        } else {
          counter--;
          if (counter < 0) {
            counter = 0;
          }
        }
        ready_to_measure = false;
        last_counted = millis();
        s1->reset();
        s2->reset();

        _counterCharacteristic->writeValue(counter);
      }
    }
    SonicSensor* s1;
    SonicSensor* s2;
    BLEIntCharacteristic* _counterCharacteristic;
    int counter = 0;
    int last_counted = -1;
    boolean ready_to_measure = true;
};



/****************************************************************************************/
/************************************** INSTANCES ***************************************/
/****************************************************************************************/

/*---------------------------------------- BLE -----------------------------------------*/
// BLE Characteristic for the service
// remote clients will be able to get notifications if this characteristic changes
BLEIntCharacteristic personCountInt(COUNT_CHARACTERISTIC_UUID, BLERead | BLENotify);

/*----------------------------------- PERSON COUNTER -----------------------------------*/
//The thread variable needs to be live after setup call
PersonCounter pc(&personCountInt);

/****************************************************************************************/
/************************************** SETUP CODE **************************************/
/****************************************************************************************/

/*------------------------------------- MAIN SETUP -------------------------------------*/
void setup() {
  // Initialize serial if active
  if (IS_SERIAL_ACTIVE) {
    Serial.begin(9600);
    while (!Serial);
  }

  // Setup connection led
  pinMode(LEDB, OUTPUT);
  digitalWrite(LEDB, HIGH);

  // Setup BLE comm with other board
  setup_ble();
}

/*------------------------------------- BLE SETUP --------------------------------------*/
void setup_ble() {
  // Begin BLE initialization
  if (!BLE.begin()) {
    printlnSerial("starting BLE failed!");
    while (1);
  }

  // Print own BLE address
  printSerial("Local address is: ");
  printlnSerial(BLE.address());

  // Set device and local name
  BLE.setLocalName(DEVICE_NAME);
  BLE.setDeviceName(DEVICE_NAME);

  // Create and set air quality service to BLE device
  BLEService personCountService(PERSON_COUNTER_SERVICE_UUID);
  BLE.setAdvertisedService(personCountService);

  // Add characteristics to service, then add service to device
  personCountService.addCharacteristic(personCountInt);
  BLE.addService(personCountService);

  // Write initial values of the characteristics
  personCountInt.writeValue(0);

  // Start advertising
  BLE.advertise();
  printlnSerial("Person counter device active, waiting for connections...");
}

void loop() {
  // Wait for a BLE central to connect
  BLEDevice central = BLE.central();
  printlnSerial("Scanning for clients...");

  // If no central connected to the peripheral
  if (!central) {
    // Restart advertising
    printlnSerial("No central found. Retrying in 5 seconds...");
    BLE.stopAdvertise();
    BLE.advertise();
    delay(5000);
    return;
  }

  // Otherwise a central is connected to the peripheral
  printSerial("Connected to central: ");
  // Print the central's BT address and turn on LED to indicate connection
  printlnSerial(central.address());
  digitalWrite(LEDB, LOW);

  // While the central is connected
  while (central.connected()) {
    pc.personCounterLoop();
  }

  // When the central disconnects, turn off the LED:
  digitalWrite(LEDB, HIGH);
  printSerial("Disconnected from central: ");
  printlnSerial(central.address());

  // Restart advertising
  BLE.stopAdvertise();
  BLE.advertise();
}
